<?php
/**
 * @file
 * Code for the MaxLengthValidator.
 */

/**
 * String Max Length validator.
 *
 * @copyright    Copyright 2013, goalgorilla.com
 * @license      http://www.goalgorilla.com/license
 * @since        version 1.0
 */
class MaxLengthValidator extends Validator {

  /**
   * Check if the value has the valid length.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {

    // Make sure we use UTF8 encoding.
    // Russian characters are 2 bytes, Thai are 3 bytes. Function strlen() does
    // not hold that into account.
    $value = utf8_decode($value);

    // The string is at least "max" characters length.
    if (isset($this->options['max'])) {
      $max = $this->options['max'];
      if (strlen($value) > $max) {
        $error = $this->setError(t('The given string "@value" cannot be longer than "@max" characters length. OVERRIDE',
          array('@value' => $value, '@max' => $max)), 'max');
      }
    }
  }

}
