<?php
/**
 * @file
 * Code for the RequiredValidator.
 */

/**
 * Required validator.
 *
 * @copyright    Copyright 2013, goalgorilla.com
 * @license      http://www.goalgorilla.com/license
 * @since        version 1.0
 */
class RequiredValidator extends Validator {

  /**
   * Check if the value has the valid length.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    $not = $this->getOption('not');
    $match = is_null($value) || (empty($value) && (!is_bool($value) && !is_numeric($value)));
    if (($not && !$match) || (!$not && $match)) {
      $this->setError(t('The field is required.'));
    }
  }
}
