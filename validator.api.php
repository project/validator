<?php
/**
 * @file
 * Describe hooks provided by the Validator API.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * This hook is executed right before the actual validation in the
 * hook_form_alter is being called. In this hook the form and form_state
 * can still be altered.
 *
 * @param array &$form
 *   The form
 * @param array &$form_state
 *   The current state of the form
 */
function hook_before_validate(&$form, &$form_state) {
  $form_id = $form['#form_id'];
}

/**
 * This hook is used to alter constraints to your specific needs
 * You can for instance alter the length of and minimum field length checker
 * Also the message that's outputted can be altered here.
 *
 * @return array
 *   Array of constraints to use during validation.
 */
function hook_constraints_alter($constraints = array()) {
  return $constraints;
}

/**
 * This hook is being executed after all the validation has taken place.
 * It's save to say the validator only executes this code when the
 * validation of teh data was successful.
 *
 * @param array &$form
 *   The form
 * @param array &$form_state
 *   The current state of the form
 */
function hook_after_validate(&$form, &$form_state) {
  $form_id = $form['#form_id'];
  if (($form_id == 'user_profile_form' || $form_id == 'user_register_form' || isset($form['#user_category'])
    && $form['#user_category'] == 'register')) {

    // If the mobile phone field has failed the validation
    // put the phone house field in error also.
    $errors = form_get_errors();
    if (isset($errors['field_user_phone_mobile'])) {
      $field_name = 'field_user_phone_house';
      $field_id = FormFieldHelper::getFieldId($field_name);
      // No message required, the field user phone house
      // take care of the message.
      form_set_error($field_id, '');
    }
  }
}

/**
 * @} End of "addtogroup hooks".
 */
