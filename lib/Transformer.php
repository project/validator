<?php
/**
 * @file
 * Code for the Transformer.
 */

/**
 * Form Transformer
 */
abstract class Transformer {
  /**
   * The original value to transform
   * @type {mixed}
   */
  protected $value = NULL;

  /**
   * Options of the transformer
   * @type {array}
   */
  protected $options = array();

  /**
   * Constructor.
   *
   * @param {array} $options
   *   Options of the transformer
   */
  public function __construct($options = array()) {
    $this->options = $options;
  }

  /**
   * Get transformer options.
   *
   * @return array
   *   The transformer's options.
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * Get a transformer's option.
   *
   * @param string $name
   *   The transformer's option name.
   *
   * @return mixed
   *   The transformer's option
   */
  public function getOption($name) {
    return is_array($this->options) && isset($this->options[$name]) ? $this->options[$name] : NULL;
  }

  /**
   * Set the transformer's value.
   *
   * @param mixed $value
   *   The transformer's value
   */
  public function setValue($value) {
    $this->value = $value;
  }

  /**
   * Load transformer functions of the given name.
   *
   * @param {string} $name
   *   The transformer's name.
   *
   * @return Transformer
   *   The Transformer relative to the given name.
   */
  public static function loadTransformer($name = '') {
    $class_name = $name . 'Transformer';
    // If the transformer is not already loaded.
    if (!class_exists($class_name)) {
      // Include the transformer class.
      if (!module_load_include('inc', 'validator', 'lib/transformer/' . $class_name) !== FALSE) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Transform the value.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value
   */
  public abstract function transform($value);
}
