<?php
/**
 * @file
 * Code for the LengthValidator.
 */

/**
 * Or validator.
 */
class OrValidator extends Validator {
  /**
   * Validate the value.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    $opts = $this->getOptions();
    $validate = NULL;

    // Nothing to test.
    if (!is_array($opts)) {
      trigger_error("OrValidator: options are missing", E_USER_WARNING);
      return;
    }
    // An array has been given.
    else {
      if (!isset($opts['constraints'])) {
        trigger_error("OrValidator: options.constraints are missing", E_USER_WARNING);
      }
      else {
        $constraints = $opts['constraints'];
        foreach ($constraints as $field_name => $field_constraints) {
          foreach ($field_constraints as $field_constraint_name => $field_constraint_opts) {
            // If the constraint has no options.
            // The constraint will be applied to the current field.
            if (is_int($field_constraint_name)) {
              $field_constraint_name = $field_constraint_opts;
              $field_constraint_opts = array();
            }
            // Extract the value to test.
            $field_value = isset($values[$field_name]) ? $values[$field_name] : NULL;

            // Load the target validator.
            Validator::loadValidator($field_constraint_name);
            $class_name = $field_constraint_name . 'Validator';
            $validator = new $class_name($field_constraint_name, $field_constraint_opts);
            // Validate the value functions of the defined validator.
            $validator->validate($field_value, $values);
            $validator_errors = $validator->getErrors();

            // Concat the result of this validation to the already performed
            // validations.
            if (is_null($validate)) {
              $validate = empty($validator_errors);
            }
            else {
              $validate = $validate || empty($validator_errors);
            }
          }
        }
      }
    }

    // If "or" has not been validated, set an error.
    if (!$validate) {
      $this->setError();
    }
  }
}
