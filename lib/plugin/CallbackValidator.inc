<?php
/**
 * @file
 * Code for the CallbackValidator.
 */

/**
 * Callback validator.
 */
class CallbackValidator extends Validator {
  /**
   * Validate the given value.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    // Method to execute.
    $method = '';
    // Arguments to pass to the method.
    $args = array();

    // If simple callback to execute.
    if (!is_array($this->options)) {
      // Method to execute.
      $method = $this->options;
      $args = array($value, $values);
    }
    // If the constraint as been defined in details trough an array.
    elseif (is_array($this->options)) {
      // Method option as to be defined.
      if (!isset($this->options['method'])) {
        throw new Exception(t('Invalid constraint option Callback:method. The method option should be filled.'));
      }
      // Override the method.
      $method = $this->options['method'];

      // If arguments are defined.
      if (isset($this->options['args'])) {
        foreach ($this->options['args'] as $arg) {
          // If complex arguments given.
          if (is_array($arg)) {
            // A complex argument should have a type.
            if (!isset($arg['type'])) {
              throw new Exception(t('Invalid constraint option Callback:method:args[]:type. This option should be filled.'));
            }
            // Function of the argument type apply a different method.
            switch ($arg['type']) {
              case 'field_value':
                array_push($args, $this->callbackArgumentFieldValue($arg, $values));
                break;
            }
          }
          // Simple argument given.
          else {
            array_push($args, $arg);
          }
        }
      }
      // Otherwise arguments will be the field value, and the form values.
      else {
        $args = array($value, $values);
      }
    }

    // The callback method should exist.
    if (!function_exists($method)) {
      throw new Exception(t('Invalid constraint option Callback:method. The method "@method" does not exist.', array('@method' => $method)));
    }

    // Execute the callback, set error if the false.
    $result = call_user_func_array($method, $args);
    if ($result !== TRUE) {
      $this->setError($result !== FALSE ? $result : NULL);
    }
  }

  /**
   * Get the field value.
   *
   * @return mixed
   *   The field's value.
   */
  protected function callbackArgumentFieldValue($arg, $values) {
    // A complex field argument type should have a field name to target.
    if (!isset($arg['name'])) {
      throw new Exception(t('Invalid constraint option Callback:method:args[]:name. This option should be filled when you use the arg type "@type".', array('@type' => $arg['type'])));
    }

    // Get the target field name and return the value.
    $target_field_name = $arg['name'];
    return $values[$target_field_name];
  }
}
