<?php
/**
 * @file
 * Code for the RegexValidator.
 */

/**
 * Regex validator.
 */
class RegexValidator extends Validator {
  /**
   * Check if the value is matching the pattern.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    $pattern = $this->getPattern();

    if (is_null($pattern)) {
      throw new Exception(t('Invalid constraint option Regex:pattern. The method option should be filled.'));
    }

    // If we want to validate the value if the pattern is not found.
    $not = $this->getOption('not');

    $pattern = '/' . $pattern . '/u';
    $match = preg_match($pattern, $value);

    if (($not && $match) || (!$not && !$match)) {
      $this->setError(t('The field value "@value" does not match the pattern "@pattern".', array(
        '@value' => $value,
        '@pattern' => $pattern,
      )));
    }
  }

  /**
   * Get the regex pattern.
   * @return string
   *   The regex pattern
   */
  protected function getPattern() {
    $pattern = NULL;
    // If options is not an array, the pattern should be given as value.
    if (!is_array($this->options)) {
      $pattern = $this->options;
    }
    elseif (isset($this->options['pattern'])) {
      $pattern = $this->options['pattern'];
    }
    return $pattern;
  }
}
