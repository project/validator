<?php
/**
 * @file
 * Code for the ChoiceValidator.
 */

/**
 * Choice validator.
 */
class ChoiceValidator extends Validator {
  /**
   * Validate the given value.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    $choices = $this->getChoices($values);

    // Test multiple choice cardinality.
    if ($this->getOption('multiple')) {
      foreach ($value as $v) {
        $this->validateChoice($v, $choices);
      }
    }
    // Test the single choice validity.
    else {
      // The value is a scalar value.
      if (is_array($value)) {
        $this->setError('MULTIPLE OPTIONS FOR A SIMPLE CHECK DOES NOT WORK');
      }
      // Check if the value is in the avaliable values.
      else {
        $this->validateChoice($value, $choices);
      }
    }
  }

  /**
   * Validate a choice.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @param array $choices
   *   The available choices.
   */
  protected function validateChoice($value, $choices) {
    $case_sensitive = $this->getOption('caseSensitive');
    $case_sensitive = !is_null($case_sensitive) ? $case_sensitive : TRUE;

    // Does not take care of the case.
    if ($case_sensitive) {
      if (!in_array($value, $choices)) {
        $this->setError();
      }
    }
    else {
      if (!in_array(strtolower($value), array_map('strtolower', $choices))) {
        $this->setError();
      }
    }
  }

  /**
   * Get available choice(s).
   *
   * @param array $values
   *   The contextual values
   *
   * @return array
   *   Array of available choices
   */
  protected function getChoices($values) {
    $choices = array();

    // A callback function provides the available values.
    if (isset($this->options['callback'])) {
      $choices = $this->options['callback']($values);
    }
    // An array of choices is provided.
    elseif (isset($this->options['choices'])) {
      $choices = $this->options['choices'];
      if ($this->options['key_to_value']) {
        $choices = array_keys($choices);
      }
    }
    // One or several drupal vocabularies (machine_name) provide the available
    // choices.
    elseif (isset($this->options['vocabulary'])) {
      if (!is_array($this->options['vocabulary'])) {
        $this->options['vocabulary'] = array($this->options['vocabulary']);
      }
      foreach ($this->options['vocabulary'] as $vocabulary_name) {
        $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name);
        $tree = taxonomy_get_tree($vocabulary->vid);
        // @todo flat the tree
        foreach ($tree as $term) {
          $choices[] = $term->tid;
        }
      }
    }

    // Return the choices.
    return $choices;
  }
}
