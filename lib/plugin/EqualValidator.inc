<?php
/**
 * @file
 * Code for the EqualValidator.
 */

/**
 * Equal validator.
 */
class EqualValidator extends Validator {
  /**
   * Check if the value is matching the pattern.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    $value_to_compare = $this->getValueToCompare();
    // Not equal option.
    $not = $this->getOption('not');
    $match = ($value === $value_to_compare);
    if (($not && $match) || (!$not && !$match)) {
      $this->setError(t('The field value "@value" does not match the value "@value_to_compare".',
        array('@value' => $value, '@value_to_compare' => $value_to_compare)));
    }
  }

  /**
   * Get the value to compare.
   * @return mixed
   *   The value to compare.
   */
  protected function getValueToCompare() {
    $value_to_compare = NULL;
    // If options is not an array, the pattern should be given as value.
    if (!is_array($this->options)) {
      $value_to_compare = $this->options;
    }
    elseif (isset($this->options['value'])) {
      $value_to_compare = $this->options['value'];
    }
    return $value_to_compare;
  }
}
