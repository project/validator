<?php
/**
 * @file
 * Code for the DrupalUsernameValidator.
 */

/**
 * Drupal Username validator.
 */
class DrupalUsernameValidator extends Validator {
  /**
   * Check if the value is a valid drupal username.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    // A user reference has been passed through the form values.
    $user = NULL;
    // Load the reference user.
    if (!empty($values['uid'])) {
      $user = user_load($values['uid']);
    }

    // If the reference user name is the same than the given value -> success.
    // Otherwise, check that the given value does not exists already.
    if (!(!is_null($user) && $user->name == $value)) {
      $user_exists = user_load_by_name($value);
      if ($user_exists !== FALSE) {
        $this->setError(t('The name "@value" is already taken, please choose another one.', array('@value' => $value)));
      }
    }

    // Set the error if the username is not valid.
    if ($value == '' || user_validate_name($value)) {
      $this->setError(t('The name "@value" is not a valid username.', array('@value' => $value)));
    }
  }
}
