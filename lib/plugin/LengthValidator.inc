<?php
/**
 * @file
 * Code for the LengthValidator.
 */

/**
 * String Length validator.
 */
class LengthValidator extends Validator {
  /**
   * Check if the value has the valid length.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    // Make sure we use UTF8 encoding.
    // Russian characters for example are 2 bytes, Thai are 3 bytes. The
    // function strlen() does not hold that into account.
    $value = utf8_decode($value);

    // The string is at least "min" characters length.
    if (isset($this->options['min'])) {
      $min = $this->options['min'];
      if (strlen($value) < $min) {
        $error = $this->setError(t('The given string "@value" should be at least "@min" characters length.',
          array('@value' => $value, '@min' => $min)), 'min');
      }
    }

    // The string is at least "max" characters length.
    if (isset($this->options['max'])) {
      $max = $this->options['max'];
      if (strlen($value) > $max) {
        $error = $this->setError(t('The given string "@value" cannot be longer than "@max" characters length.',
          array('@value' => $value, '@min' => $max)), 'max');
      }
    }
  }
}
