<?php
/**
 * @file
 * Code for the RequiredUserFieldValidator.
 */

/**
 * RequiredUserField validator.
 */
class RequiredUserFieldValidator extends Validator {
  /**
   * Check if the value is filled.
   *
   * * null = false
   * * !isset = false
   * * empty string = false
   * * empty array = false
   * * false = true
   * * numeric value 0 or 0.0 = true
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    $not = $this->getOption('not');
    $match = is_null($value) || (empty($value) && (!is_bool($value) && !is_numeric($value)));

    // Check for errors.
    if (($not && !$match) || (!$not && $match)) {
      $this->setError(t('The field is required.'));
    }
  }
}
