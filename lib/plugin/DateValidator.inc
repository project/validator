<?php
/**
 * @file
 * Code for the DateValidator.
 */

/**
 * Date validator.
 */
class DateValidator extends Validator {
  /**
   * Validate the given value.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    // The date is a valid date.
    // EXPECTED FORMAT YYYY/mm/dd HH:ii:ss
    if (!$this->isValidDate($value)) {
      $this->setError(t('Invalid date "@value". Format should be "Y-m-d H:i:s"', array('@value' => $value)));
    }

    // The date should be greater than.
    if (isset($this->options['greater'])) {
      $greater = $this->options['greater'];
      if (!$this->isValidDate($greater)) {
        throw new Exception(t('Invalid constraint option Date:greater "@greater". Format should be "Y-m-d H:i:s"', array('@greater' => $greater)));
      }
      // Check the given date is greater than the date given in the constraint
      // definition.
      if ($value < $greater) {
        $error = $this->setError(t('The given date "@value" should be greater than the date "@greater".', array('@value' => $value, '@greater' => $greater)), 'greater');
      }
    }

    // The date should be lower than.
    if (isset($this->options['lower'])) {
      $lower = $this->options['lower'];
      if (!$this->isValidDate($lower)) {
        throw new Exception(t('Invalid constraint option Date:lower "@lower". Format should be "Y-m-d H:i:s"', array('@lower' => $lower)));
      }
      // Check the given date is lower than the date given in the constraint
      // definition.
      if ($value > $lower) {
        $error = $this->setError(t('The given date "@value" should be lower than the date "@lower".', array('@value' => $value, '@lower' => $lower)), 'lower');
      }
    }
  }

  /**
   * Check if the date is a valid datetime.
   *
   * @param string $date
   *   The date to test.
   *
   * @return bool
   *   Return TRUE if the it is a valide date, otherwise FALSE.
   */
  protected function isValidDate($date) {
    if (date('Y-m-d H:i:s', strtotime($date)) == $date) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}
