<?php
/**
 * @file
 * Code for the DrupalUserMailValidator.
 */

/**
 * Drupal User Mail validator.
 */
class DrupalUserMailValidator extends Validator {
  /**
   * Check if the value is a valid drupal username.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    // A user reference has been passed through the form values.
    $user = NULL;
    // Load the reference user.
    if (!empty($values['uid'])) {
      $user = user_load($values['uid']);
    }

    // If the reference user mail is the same than the given value -> success.
    // Otherwise, check that the given value does not exists already.
    if (!(!is_null($user) && $user->mail == $value)) {
      // If a user already exists for the given mail.
      $user_exists = user_load_by_mail($value);
      if ($user_exists !== FALSE) {
        $this->setError(t('The mail "@value" is already taken, please choose another one.', array('@value' => $value)));
      }
    }

    // Check that the value is a valid email.
    Validator::loadValidator('Email');
    $validator = new EmailValidator('Email');
    $validator->validate($value);
    $errors = $validator->getErrors();

    // Check if there are errors, if so let the user know.
    if (!empty($errors)) {
      $this->setError(t('The mail "@value" is not valid.', array('@value' => $value)));
    }
  }
}
