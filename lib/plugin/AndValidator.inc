<?php
/**
 * @file
 * Code for the AndValidator.
 */

/**
 * And validator.
 */
class AndValidator extends Validator {
  /**
   * Validate the given value.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    $opts = $this->getOptions();
    $validate = NULL;

    // Nothing to test.
    if (!is_array($opts)) {
      trigger_error("AndValidator: options are missing", E_USER_WARNING);
      return;
    }
    // An array has been given.
    else {
      if (!isset($opts['constraints'])) {
        trigger_error("AndValidator: options.constraints are missing", E_USER_WARNING);
      }
      else {
        $constraints = $opts['constraints'];
        foreach ($constraints as $field_name => $field_constraints) {
          foreach ($field_constraints as $field_constraint_name => $field_constraint_opts) {
            // If the constraint has no options.
            // The constraint will be applied to the current field.
            if (is_int($field_constraint_name)) {
              $field_constraint_name = $field_constraint_opts;
              $field_constraint_opts = array();
            }
            $field_value = isset($values[$field_name]) ? $values[$field_name] : NULL;
            Validator::loadValidator($field_constraint_name);
            $class_name = $field_constraint_name . 'Validator';
            $validator = new $class_name($field_constraint_name, $field_constraint_opts);
            $validator->validate($field_value, $values);
            $validator_errors = $validator->getErrors();

            if (is_null($validate)) {
              $validate = empty($validator_errors);
            }
            else {
              $validate = $validate && empty($validator_errors);
            }
          }
        }
      }
    }
    // If "and" has not been validated.
    if (!$validate) {
      $this->setError();
    }
  }
}
