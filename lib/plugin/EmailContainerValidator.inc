<?php
/**
 * @file
 * Code for the EmailContainer.
 */

/**
 * EmailContainer validator.
 */
class EmailContainerValidator extends Validator {
  /**
   * Check if the value contains a string matching the pattern.
   * @see parent::validate
   */
  public function validate($value = NULL, &$values = array()) {
    // The text to verify if it contains a pattern.
    $text = NULL;
    // The regex to match.
    $pattern = '';

    // Search for email addresses.
    $pattern = '[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})';

    $pattern = '/' . $pattern . '/i';
    preg_match($pattern, $value, $matches);

    if (isset($matches[0])) {
      $this->setError(t('Please do not insert any email address in the explanation, you will be asked for it in the next step.'));
    }
  }
}
