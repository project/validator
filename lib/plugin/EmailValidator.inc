<?php
/**
 * @file
 * Code for the EmailValidator.
 */

/**
 * Email validator.
 *
 * @copyright    Copyright 2013, goalgorilla.com
 * @license      http://www.goalgorilla.com/license
 * @since        version 1.0
 */
class EmailValidator extends Validator {
  /**
   * Check if the value is a valid email.
   * @see parent::validate
   * @see drupal::valid_email_address
   */
  public function validate($value = NULL, &$values = array()) {
    if ($value == '' || !valid_email_address($value)) {
      $this->setError(t('The email "@value" is not a valid email.', array('@value' => $value)));
    }
  }
}
