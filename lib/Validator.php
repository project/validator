<?php
/**
 * @file
 * Code for the Validator.
 */

/**
 * Property validator.
 */
abstract class Validator {
  /**
   * Name of the constraint
   * @type {string}
   */
  protected $name = NULL;

  /**
   * The value to test
   * @type {mixed}
   */
  protected $value = NULL;

  /**
   * Options of the constraint
   * @type {mixed}
   */
  protected $options = NULL;

  /**
   * Errors when a value has been validated
   * @type {array}
   */
  private $errors = array();

  /**
   * Default error message
   * @type {string}
   */
  protected $defaultErrorMessage = '';

  /**
   * Constructor.
   *
   * @param string $name
   *   Name of the constraint.
   *
   * @param array $options
   *   Options of the constraint.
   */
  public function __construct($name = '', $options = array()) {
    $this->name = $name;
    $this->options = $options;
  }

  /**
   * Get constraint name.
   *
   * @return string
   *   The constraint name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get a validator's option.
   *
   * @param string $name
   *   The validator's option name.
   *
   * @return mixed
   *   The validator's option
   */
  public function getOption($name) {
    return is_array($this->options) && isset($this->options[$name]) ? $this->options[$name] : NULL;
  }

  /**
   * Get validator's options.
   *
   * @return array
   *   The validator's options.
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * Get errors after validation.
   *
   * @return array
   *   Array of errors.
   */
  public function getErrors() {
    return $this->errors;
  }

  /**
   * Load validator functions of its constraint name.
   *
   * @param string $name
   *   The constraint name.
   *
   * @return Validator
   *   The Validator relative to the given name.
   */
  public static function loadValidator($name = '', $module = FALSE) {
    $class_name = $name . 'Validator';
    // If the validator is not already loaded.
    if (!class_exists($class_name)) {
      // Include contributed modules first, so we can even override validators.
      if (!module_load_include('inc', $module, 'inc/' . $class_name)) {
        // Include the validator class.
        if (!module_load_include('inc', 'validator', 'lib/plugin/' . $class_name) !== FALSE) {
          return FALSE;
        }
        return TRUE;
      }
      return TRUE;
    }
    return TRUE;
  }

  /**
   * Add an error. Following the priorities.
   *
   * 1. A message is defined in constraint option;
   * 2. A message is passed to the function;
   * 3. A default message has been defined as class variable;
   * 4. Generate a default message;
   *
   * @param string $msg
   *   Add an error message.
   *
   * @param string $case
   *   Error is relative to a specific case.
   */
  protected function setError($msg = '', $case = '') {
    $error = '';
    $msg_vars = array(
      '@name' => $this->getName(),
    );

    // A message is defined in constraint option.
    if (isset($this->options['message']) && (
        (!empty($case) && $this->options['message'][$case])
        || empty($case)
      )
    ) {
      // A message is contextualised.
      if (!empty($case)) {
        $error = t($this->options['message'][$case], $msg_vars);
      }
      else {
        $error = t($this->options['message'], $msg_vars);
      }

    }
    // A message is passed to the function.
    elseif (!empty($msg)) {
      $error = $msg;
    }
    // A default message has been defined as class variable.
    elseif (!empty($this->defaultErrorMessage)) {
      $error = t($this->defaultErrorMessage, $msg_vars);
    }
    // Generate a default message.
    else {
      $error = t('Unable to validate the field with the constraint "@name"', $msg_vars);
    }

    array_push($this->errors, $error);
  }

  /**
   * Validate the given value.
   *
   * @param array $value
   *   Value to validate.
   *
   * @param array $values
   *   The contextual values.
   */
  public abstract function validate($value, &$values = array());
}
