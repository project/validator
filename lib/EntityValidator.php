<?php
/**
 * @file
 * Code for the Entity Validator.
 */

/**
 * Entity validator.
 */
class EntityValidator {
  /**
   * Errors
   *
   * @var array
   */
  protected $errors = array();

  /**
   * Definitions
   *
   * @var array
   */
  protected static $definitions = array();

  /**
   * Get errors after validation.
   *
   * @return array
   *   Array of errors.
   */
  public function getErrors() {
    return $this->errors;
  }

  /**
   * Get the constraints definitions to apply to the given form.
   *
   * @param string $entity_type
   *   The target entity type.
   *
   * @param string $entity_bundle
   *   The target entity bundle.
   *
   * @param array $options
   *   Optional parameters. We will use it to define options such as domain.
   *   <code>
   *     array (
   *      'domain' => 'nl'
   *     )
   *    </code>
   *
   * @return array
   *   Array of constraints definitions
   */
  public function getConstraintsDefinitions($entity_type = '', $entity_bundle = '') {
    $definitions = array();

    // Construct hook name.
    if (!empty($entity_type)) {
      // Check if we have a entity bundle we can do something with.
      if (!empty($entity_bundle)) {
        // Check if there are modules implementing this hook.
        if (sizeof(module_implements('validator_entity_' . $entity_type . '_' . $entity_bundle . '_constraints')) > 0) {
          $definitions = module_invoke_all('validator_entity_' . $entity_type . '_' . $entity_bundle . '_constraints', $definitions);
        }
      }
      else {
        // Check if there are modules implementing this hook.
        if (sizeof(module_implements('validator_entity_' . $entity_type . '_constraints')) > 0) {
          $definitions = module_invoke_all('validator_entity_' . $entity_type . '_constraints', $definitions);
        }
      }

      // Let other module alter the constraints based upon the entity type only.
      drupal_alter('validator_entity_' . $entity_type . '_constraints', $definitions);

      // If we have a entity bundle specified, we should also alter that one.
      if (!empty($entity_bundle)) {
        drupal_alter('validator_entity_' . $entity_type . '_' . $entity_bundle . '_constraints', $definitions);
      }
    }

    return $definitions;
  }

  /**
   * Validate the given values with the given constraints definitions.
   *
   * @param array $definitions
   *   Array of constraints definitions.
   *
   * @param array $values
   *   The values to validate.
   */
  public function validate($definitions = array(), &$values = array()) {
    // For each constraints definitions.
    foreach ($definitions as $field_name => $definition) {
      if (isset($definition['constraints'])) {
        // Get the field value.
        $field_value = $values[$field_name];
        // Validate the field.
        $this->validateField($definition, $field_name, $field_value, $values);
      }
    }
  }

  /**
   * Check if a field has a target constraint.
   *
   * @param array $definition
   *   Field constraints definition.
   *
   * @param string $field_name
   *   The target field.
   *
   * @param string $constraint_name
   *   The target constraint name.
   *
   * @return bool
   *   Return true if a constraint has been defined for the given name.
   */
  public function fieldHasConstraint($definition, $field_name, $constraint_name) {
    $return = FALSE;
    // The constraint is defined without options.
    if (in_array('Required', $definition['constraints'])) {
      $return = TRUE;
    }
    // The constraint has been defined with options.
    elseif (isset($definition['constraints']['Required'])) {
      if ($definition['constraints']['Required'] !== FALSE) {
        $return = TRUE;
      }
    }
    return $return;
  }

  /**
   * Sort the array of constraints by weight.
   *
   * @param array $c1
   *   The first constraint to compare.
   *
   * @param array $c2
   *   The second constraint to compare.
   *
   * @return int
   *   -1 if weight of c1 is inf than weight of c2, otherwise 1
   */
  public function sortByWeight($c1, $c2) {
    $c1_weight = is_array($c1) && isset($c1['weight']) ? $c1['weight'] : 1;
    $c2_weight = is_array($c2) && isset($c2['weight']) ? $c2['weight'] : 1;
    if ($c1_weight < $c2_weight) {
      return -1;
    }
    elseif ($c1_weight > $c2_weight) {
      return 1;
    }
    else {
      return 1;
    }
  }

  /**
   * Validate the given field with the given constraints definition.
   *
   * @param array $definition
   *   Array of constraints definitions.
   *
   * @param string $field_name
   *   The target field to validate.
   *
   * @param string $field_value
   *   The target field value to validate.
   *
   * @param array $form_values
   *   The form values.
   */
  public function validateField($definition = array(), $field_name = '', $field_value = NULL, &$form_values = array()) {
    if (isset($definition['constraints'])) {
      // Compulsory case to check for each field.
      // If the field is not required and the field is empty the system won't
      // test other constraints.
      $not_required_case_passed = FALSE;
      // Sort the constraints by weight.
      uasort($definition['constraints'], array(
        $this,
        'sortByWeight',
      ));

      // Validate the form field for each constraint defined for it.
      foreach ($definition['constraints'] as $constraint_name => $constraint_options) {
        // If the array key is an int, the constraint name is defined by the
        // item value.
        if (is_int($constraint_name)) {
          $constraint_name = $constraint_options;
          $constraint_options = array();
        }
        // Get the current weight or give an automatic weight.
        $weight = isset($constraint_options['weight']) ? $constraint_options['weight'] : 1;

        $validator = $this->constraintValidatedBy($constraint_name, $constraint_options);
        // Validate the field with the current constraint.
        $validator->validate($field_value, $form_values);

        // If error.
        $errors = $validator->getErrors();
        if (!empty($errors)) {
          // No error already found for the tested field.
          if (empty($this->errors[$field_name])) {
            $this->errors[$field_name] = array();
          }
          $this->errors[$field_name][$constraint_name] = $errors[0];
        }
      }
    }
  }

  /**
   * Get the validator which will be used to validate the given constraint.
   *
   * @param string $name
   *   The target constraint name.
   *
   * @param array $options
   *   The constraint options.
   *
   * @return Validator
   *   Return the validator which is relative to the given constraint name.
   */
  public function constraintValidatedBy($name = '', $options = array(), $module = FALSE) {
    $class_name = $name . 'Validator';

    if (isset($options['file'])) {
      $module = $options['file'];
    }

    Validator::loadValidator($name, $module);
    // If the no validator class found for the given class.
    if (!class_exists($class_name)) {
      throw new Exception(t('No Validator found for the constraint "@constraintName"', array('@constraintName' => $name)));
    }
    // Return an instance of the constraint validator.
    return new $class_name($name, $options);
  }
}
