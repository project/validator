<?php

/**
 * @file
 * Code for the Form Field Helper.
 */

/**
 * Form Field Helper
 *
 * @copyright    Copyright 2013, goalgorilla.com
 * @license      http://www.goalgorilla.com/license
 * @since        version 1.0
 */
class FormFieldHelper {
  /**
   * Get the field id.
   *
   * @param string $name
   *   The target field name.
   * @param bool $error
   *   Return error fieldname specifically for location fields.
   *
   * @return string
   *   The field id.
   */
  public static function getFieldId($name = '', $error = FALSE) {
    // If the name is composite "location.country".
    $exploded_var = explode('.', $name);

    if ($error === TRUE && $exploded_var[0] == 'field_location') {
      return $exploded_var[0] . '][und][0][' . $exploded_var[1];
    }

    if (count($exploded_var) > 1 && $error === TRUE) {
      return $exploded_var[0] . '][und][0][' . $exploded_var[1];
    }

    // The first element defines the field id, otherwise the name
    // parameter defines the field id.
    return count($exploded_var) > 1 ? $exploded_var[0] : $name;
  }

  /**
   * Get the field value key.
   *
   * @param string $name
   *   The target field name.
   *
   * @param array $types
   *   Array of types.
   *
   * @return string
   *   The field value id.
   */
  public static function getFieldValueKey($name = '', $types = array()) {
    // By default the value key is defined by 'value'.
    $key = 'value';

    // If the name is composite "location.country"
    // The second element defines the value.
    $exploded_var = explode('.', $name);
    if (count($exploded_var) > 1) {
      if ($exploded_var[0] == 'location.country') {
        $key = $exploded_var[1];
      }
      else {
        $key = 'value';
      }
    }

    // If the field is a term.
    if (in_array('term', $types)) {
      $key = 'tid';
    }

    return $key;
  }

  /**
   * Extract the value of the form field.
   *
   * @param string $field_name
   *   The target field name.
   *
   * @param array $definition
   *   The field definition.
   *
   * @param array $form_state
   *   The target form state.
   *
   * @return mixed
   *   The value of the form field.
   */
  public static function getFieldValue($field_name = '', $definition = array(), &$form_state = array()) {
    $value = NULL;
    // Extracting the field value will depend of the field type, for now just
    // check property and field type.
    // Checking field collection will be more tuf, see it later if required.
    $types = isset($definition['type']) && is_array($definition['type']) ? $definition['type'] : array('property');
    $field_id = FormFieldHelper::getFieldId($field_name);

    $field_value_key = FormFieldHelper::getFieldValueKey($field_name, $types);

    // @TODO maybe a value extractor class?
    // The field is a term.
    if (in_array('term', $types)) {
      // Multiple term choices.
      if (in_array('multiple', $types)) {
        $value = array();
        foreach ($form_state['values'][$field_id][LANGUAGE_NONE] as $term) {
          if (!is_null($term['tid'])) {
            $value[] = $term['tid'];
          }
        }
      }
      // Single term choice.
      else {
        if (isset($form_state['values'][$field_id][LANGUAGE_NONE][0]['tid'])) {
          $value = $form_state['values'][$field_id][LANGUAGE_NONE][0]['tid'];
          if ($value == 'autocreate') {
            $value = $form_state['values'][$field_id][LANGUAGE_NONE][0]['name'];
          }
        }
      }
    }
    // The value is stored following the drupal field style.
    elseif (in_array('field', $types)) {

      $exploded_var = explode('.', $field_name);
      // Check if the field is part of a field collection. If it is the path
      // to the value needs to be created in a different way than a field.
      if (count($exploded_var) > 1) {
        if ($form_state['field'][$exploded_var[0]][LANGUAGE_NONE]['field']['type'] == 'field_collection') {
          $field_id1 = $exploded_var[0];
          $field_id2 = $exploded_var[1];
          $value = $form_state['values'][$field_id1][LANGUAGE_NONE][0][$field_id2][LANGUAGE_NONE][0][$field_value_key];
        }
        else {
          $value = $form_state['values'][$field_id][LANGUAGE_NONE][0][$field_value_key];
        }
      }
      else {
        $value = $form_state['values'][$field_id][LANGUAGE_NONE][0][$field_value_key];
      }
    }
    // The value is stored following the drupal property style.
    elseif (in_array('property', $types)) {
      $value = $form_state['values'][$field_id];
    }
    // @todo maybe a value transformator class?
    // In some case the value require a transformation.
    if (in_array('date', $types)) {
      $value = str_replace('T', ' ', $value);
    }

    return $value;
  }

  /**
   * Set the field's value.
   *
   * @param string $field_name
   *   The target field name.
   *
   * @param array $definition
   *   The field definition.
   *
   * @param array $form_state
   *   The target form state.
   *
   * @return mixed
   *   The value of the form field.
   */
  public static function setFieldValue($field_name = '', $definition = array(), &$form_state = array(), $form = array(), $value = NULL) {
    $types = isset($definition['type']) && is_array($definition['type']) ? $definition['type'] : array('property');

    $field_id = FormFieldHelper::getFieldId($field_name);
    $field_value_key = FormFieldHelper::getFieldValueKey($field_name, $types);

    // The value is stored following the drupal field style.
    if (in_array('field', $types)) {
      $form_state['values'][$field_id][LANGUAGE_NONE][0][$field_value_key] = $value;
    }
    // The value is stored following the drupal property style.
    elseif (in_array('property', $types)) {
      $form_state['values'][$field_id] = $value;
    }

    return $value;
  }
}
