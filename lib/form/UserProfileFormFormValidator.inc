<?php
/**
 * @file
 * Helper function that extracts the user object from the form values.
 */

// Include the form validator class.
module_load_include('php', 'validator', 'lib/FormValidator');

/**
 * User Profile Form validator.
 */
class UserProfileFormFormValidator extends FormValidator {
  /**
   * The operations to validate on.
   * If empty the form validator will be applied on all operations.
   */
  protected $operations = array('Save');

  /**
   * Extract form values.
   * @see parent::extractFormValues
   */
  public function extractFormValues(&$definitions = array(), &$form_state = array(), &$form = array()) {
    $values = parent::extractFormValues($definitions, $form_state, $form);
    // Add the user id if this one has been set.
    if (isset($form['#user'])) {
      $values['uid'] = $form['#user']->uid;
    }
    return $values;
  }
}
