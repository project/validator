<?php
/**
 * @file
 * Code for the Form Validator.
 */

// Include the form validator class.
module_load_include('php', 'validator', 'lib/EntityValidator');
// Include the form field helper class.
module_load_include('inc', 'validator', 'lib/FormFieldHelper');

/**
 * Form validator
 */
class FormValidator extends EntityValidator {
  /**
   * The operations to validate on.
   * If empty the form validator will be applied on all operations.
   */
  protected $operations = array();

  /**
   * Get form validator.
   *
   * @param string $form_id
   *   The target form.
   *
   * @static
   */
  public static function getFormValidator($form_id = NULL) {
    $class_name = ucfirst(camelize($form_id)) . 'FormValidator';
    if (class_exists($class_name)) {
      return new $class_name();
    }
    elseif (module_load_include('inc', 'validator', 'lib/form/' . $class_name) != FALSE) {
      return new $class_name();
    }
    else {
      return new FormValidator();
    }
  }

  /**
   * Check the given operation has to be validated.
   *
   * @param string $op
   *   The target operation.
   *
   * @return bool
   *   True if the form validator can be applied on the given operation,
   *   false else.
   */
  public function hasToValidateOperation($op) {
    if (empty($this->operations)) {
      return TRUE;
    }
    // Check if the operation has to be validated.
    // The operation is translated.
    foreach ($this->operations as $op_to_validate) {
      if ($op == t($op_to_validate)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get the constraints definitions to apply to the given entity.
   *
   * @param string $form_id
   *   The target form.
   *
   * @param array $options
   *   Optional parameters. We will use it to define options such as domain.
   *   <code>
   *     array (
   *       'domain' => 'nl'
   *     )
   *   </code>
   *
   * @return array
   *   Array of constraints definitions
   */
  public function getConstraintsDefinitions($form_id = NULL, $options = array()) {
    $definitions = array();

    if (!empty($form_id)) {
      // Construct hook name.
      $hook = 'validator_form_' . $form_id . '_constraints';

      // Check if there are modules implementing this hook.
      if (sizeof(module_implements($hook)) > 0) {
        // Get all the constraints for this form.
        $definitions = module_invoke_all($hook);

        // Let other module alter the constraints.
        drupal_alter($hook, $definitions);
      }
    }

    return $definitions;
  }

  /**
   * Validate the given form with the given constraints definitions.
   *
   * @param array $definitions
   *   Array of constraints definitions.
   *
   * @param array $form_state
   *   The form state contains the form to validate values.
   *
   * @param array $form
   *   The form to validate.
   */
  public function validate($definitions = array(), &$form_state = array(), &$form = array()) {
    // Extract form values.
    $form_values = $this->extractFormValues($definitions, $form_state, $form);
    parent::validate($definitions, $form_values);

    // After validate.
    // Apply value transformation if no error on the field.
    foreach ($definitions as $field_name => $definition) {
      if (isset($definition['transformer'])) {
        $value = FormFieldHelper::getFieldValue($field_name, $definition, $form_state, $form);
        $value = $this->transformValue($definition['transformer'], $value);

        if (!isset($this->errors[$field_name])) {
          $value = FormFieldHelper::setFieldValue($field_name, $definition, $form_state, $form, $value);
        }
      }
    }
  }

  /**
   * Validate the given field with the given constraints definition.
   *
   * @param array $definition
   *   Array of constraints definitions.
   *
   * @param string $field_name
   *   The target field to validate.
   *
   * @param string $field_value
   *   The target field value to validate.
   *
   * @param array $form_values
   *   The form values.
   */
  public function validateField($definition = array(), $field_name = '', $field_value = NULL, &$form_values = array()) {
    // Validate the field.
    parent::validateField($definition, $field_name, $field_value, $form_values);

    // If an error occured.
    if (isset($this->errors[$field_name])) {
      foreach ($this->errors[$field_name] as $error) {
        // Get the field id (field_location) from the field name
        // (field_location.country) used to display error.
        $field_id = FormFieldHelper::getFieldId($field_name, TRUE);
        form_set_error($field_id, $error);
      }
    }
    else {
      $form_values[$field_name] = $field_value;
    }
  }

  /**
   * Extract form values.
   *
   * Note that only the fields which are defined in the constraints
   * definitions files are extracted.
   *
   * @param array $definitions
   *   Array of constraints definitions.
   *
   * @param array $form_state
   *   The form state contains the form to extract the values.
   *
   * @param array $form
   *   The form to validate.
   *
   * @return array
   *   Array of form fields values
   */
  public function extractFormValues(&$definitions = array(), &$form_state = array(), &$form = array()) {
    $values = array();
    // For each constraints definitions.
    foreach ($definitions as $field_name => $definition) {
      $value = FormFieldHelper::getFieldValue($field_name, $definition, $form_state, $form);

      // If some transformer have to be applied to the value.
      if (isset($definition['transformer'])) {
        $value = $this->transformValue($definition['transformer'], $value);
      }

      $values[$field_name] = $value;
    }
    return $values;
  }

  /**
   * Apply transformers to a value.
   *
   * @param array $transformers
   *   Array of transformers to apply to the value.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   */
  public function transformValue($transformers = array(), $value = NULL) {
    $return = NULL;

    foreach ($transformers as $transformer_name => $transformer_options) {
      if (is_int($transformer_name)) {
        $transformer_name = $transformer_options;
        $transformer_options = array();
      }

      Transformer::loadTransformer($transformer_name);
      $class_name = $transformer_name . 'Transformer';
      $transformer = new $class_name($transformer_options);
      $return = $transformer->transform($value);
    }

    return $return;
  }
}
