<?php
/**
 * @file
 * Code for the RegexTransformer.
 */

/**
 * Regex Transformer.
 */
class RegexTransformer extends Transformer {
  /**
   * Check if required field name is filled.
   * @see parent::transform
   */
  public function transform($value = NULL) {
    $return = NULL;
    $this->setValue($value);

    $pattern = $this->getOption('pattern');
    $replacement = $this->getOption('replacement');
    $return = preg_replace($pattern, $replacement, $value);
    return $return;
  }
}
