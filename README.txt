-- SUMMARY --

This module provides an advanced and flexible way for developers to validate
forms, entities and fields.
Using a simple system of hooks and user defined constraints everything in Drupal
can be validated.

For a full description of the module, visit the project page:
  http://drupal.org/project/validator

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/validator


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CUSTOMIZATION --

* Define your own constraints and interact with the Validator by using the hooks
  in validator.api.php


-- CONTACT --

Current maintainers:
* Ronald te Brake (ronaldtebrake) - http://drupal.org/u/ronaldtebrake
* Jochem van Nieuwenhuijsen (jochemvn) - http://drupal.org/u/jochemvn
* Bram ten Hove (bramtenhove) - http://drupal.org/u/bramtenhove
* Jaap Jan Koster (jaapjan) - http://drupal.org/u/jaapjan
* GoalGorilla (goalgorilla) - http://drupal.org/u/goalgorilla
